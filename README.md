WeClock export data column information

* Unique random identifier per device generated on app install
* Incremental numeric record identifier
* Timestamp per record in UTC (“Jan 11, 2020 9:58:31 AM”)
* GPS data with latitude, longitude, resolution,speed,altitude, bearing 40.7485964,-73.9933339,17.066,0.0,-24.600000381469727,0.0
* Geofence “home” and “work” entered, exit timestamps
* Step count incremental (10234)
* Screen on/off times
* Motion (accelerometer speed value “6.1847124”)
* Light readings (“0.042308826” lumens)
* Sound (decibel 47.83747196)
* Temperature (internal device, or rarely ambient, C/F)
