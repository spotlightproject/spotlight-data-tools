
library(dplyr)
library(lubridate)

setwd("/home/n8fr8/dev/projects/spotlight/spotlight-rwdsu-data/")
print(getwd())

# Import the data from one file
#spotdata1 <- read.csv(file = 'spotlight-rwdsu-data/rwdsu-device1-report1579635715337.csv')

# or merge all CSV in directory
csvList <- lapply(list.files("./"), read.csv, stringsAsFactors = F)
spotdata <- do.call(rbind, csvList)

head(spotdata)

#ensure date time is encoded properly
spotdata$EventDate = as.POSIXct(spotdata$EventDate,tz = "EST",format="%b %d, %Y %I:%M:%S %p")

#list first six rows
head(spotdata)

#get the step data
stepData <- subset(spotdata, EventType == "steps")
stepData$EventValue <- as.integer(stepData$EventValue)

#calculate max steps from this data
maxSteps <- max(stepData$EventValue)
print(maxSteps)

#get the screen on events
screenData <- subset(spotdata, EventType == "screen" & EventValue == "on")

#count screen on event
screenOn <- nrow(screenData)
print(screenOn)

#extract just the hour maker, so we can count by hour
screenData$EventHour <- hour(screenData$EventDate)

#show screen events between start and stop hour
with( screenData , screenData[ hour( screenData$EventDate ) >= 8 & hour( screenData$EventDate ) < 10 , ] )

#get commute data
commuteData <- subset(spotdata, EventType == "commute")

#average all the commute values, device by 1000ms value to get commute time in minutes
commuteMean <- mean(as.integer(commuteData$EventValue)/1000)
print(commuteMean)

#shortest commute
commuteMin <- min(as.integer(commuteData$EventValue)/1000)
print(commuteMin)

#longest commute
commuteMax <- max(as.integer(commuteData$EventValue)/1000)
print(commuteMax)

#get geo events and print the mean, min and max accuracy
retval <- subset(spotdata, EventType == "geo_logging")
geoparts = t(as.data.frame(strsplit(as.character(retval$EventValue),",")))
geoaccuracy = mean(as.double(geoparts[,3]))
print(geoaccuracy)
geoaccuracy = min(as.double(geoparts[,3]))
print(geoaccuracy)
geoaccuracy = max(as.double(geoparts[,3]))
print(geoaccuracy)

#display lat, lon
head(geoparts[,1:2])

#print data and count columns and rows
print(is.data.frame(spotdata))
print(ncol(spotdata))
print(nrow(spotdata))

#summarize the data
str(spotdata)

spotdata %>% group_by(EventType) %>%
  summarize(Representation = n_distinct(ID, EventDate), Count = n())

